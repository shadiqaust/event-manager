package rahman.university.ul.eventmanager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import rahman.university.ul.eventmanager.activity.usermanager.UserLoginActivity;
import rahman.university.ul.eventmanager.activity.usermanager.UserRegistrationActivity;


@RunWith(AndroidJUnit4.class)
public class UserRegistrationActivityTest {

    private UserRegistrationActivity userRegistrationActivity;

    @Rule
    public final ActivityTestRule<UserRegistrationActivity> act = new ActivityTestRule<UserRegistrationActivity>(UserRegistrationActivity.class);


    @Before
    public void setUp() throws Exception {
        userRegistrationActivity = act.getActivity();

    }

    @After
    public void tearDown() throws Exception {
        userRegistrationActivity.finish();

    }

    @Test
    public void testOnCreate() throws Exception {
        Assert.assertEquals("register", ((Button) userRegistrationActivity.findViewById(R.id.sign_up_button)).getText().toString().toLowerCase());
        Assert.assertEquals("name", ((EditText) userRegistrationActivity.findViewById(R.id.name)).getHint().toString().toLowerCase());
        Assert.assertEquals("phone", ((EditText) userRegistrationActivity.findViewById(R.id.phone)).getHint().toString().toLowerCase());
        Assert.assertEquals("email", ((EditText) userRegistrationActivity.findViewById(R.id.email)).getHint().toString().toLowerCase());
        Assert.assertEquals("password", ((EditText) userRegistrationActivity.findViewById(R.id.password)).getHint().toString().toLowerCase());
    }


}