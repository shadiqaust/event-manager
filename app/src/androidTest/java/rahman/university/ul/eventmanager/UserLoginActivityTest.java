package rahman.university.ul.eventmanager;


import android.widget.Button;
import android.widget.TextView;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


import rahman.university.ul.eventmanager.activity.usermanager.UserLoginActivity;


@RunWith(AndroidJUnit4.class)
public class UserLoginActivityTest {

    private UserLoginActivity userLoginActivity;

    @Rule
    public final ActivityTestRule<UserLoginActivity> act = new ActivityTestRule<UserLoginActivity>(UserLoginActivity.class);


    @Before
    public void setUp() throws Exception {
        userLoginActivity = act.getActivity();

    }

    @After
    public void tearDown() throws Exception {
        userLoginActivity.finish();

    }

    @Test
    public void testOnCreate() throws Exception {
        Assert.assertEquals("Event Manager", (userLoginActivity.getTitle().toString()));
        Assert.assertEquals("login", ((Button) userLoginActivity.findViewById(R.id.btn_login)).getText().toString().toLowerCase());
        Assert.assertEquals("forgot your password?", ((TextView) userLoginActivity.findViewById(R.id.btn_reset_password)).getText().toString().toLowerCase());
    }



}