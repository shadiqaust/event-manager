package rahman.university.ul.eventmanager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import rahman.university.ul.eventmanager.activity.usermanager.ResetPasswordActivity;
import rahman.university.ul.eventmanager.activity.usermanager.UserLoginActivity;


@RunWith(AndroidJUnit4.class)
public class ResetPasswordActivityTest {

    private ResetPasswordActivity resetPasswordActivity;

    @Rule
    public final ActivityTestRule<ResetPasswordActivity> act = new ActivityTestRule<ResetPasswordActivity>(ResetPasswordActivity.class);


    @Before
    public void setUp() throws Exception {
        resetPasswordActivity = act.getActivity();

    }

    @After
    public void tearDown() throws Exception {
        resetPasswordActivity.finish();

    }

    @Test
    public void testOnCreate() throws Exception {
        Assert.assertEquals("Event Manager", (resetPasswordActivity.getTitle().toString()));
        Assert.assertEquals("forgot password?", ((TextView) resetPasswordActivity.findViewById(R.id.reset_title)).getText().toString().toLowerCase());
        Assert.assertEquals("email", ((EditText) resetPasswordActivity.findViewById(R.id.email)).getHint().toString().toLowerCase());
    }


}