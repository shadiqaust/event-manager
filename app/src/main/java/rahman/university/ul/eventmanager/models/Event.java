package rahman.university.ul.eventmanager.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@IgnoreExtraProperties
public class Event implements Serializable, Parcelable {

    String key,title,image, description,video;
    int category;
    Date eventAddedDate;
    Date eventDate;
    User eventAddedBy;
    EventLocation location;
    List<String> sharedUserId;
    List<String> likedId;
    List<Comment> comments;
    boolean isPublicEvent;

    public Event(){}

    public Event(String title, String image,String video, String description,int category, EventLocation location, Date eventDate, boolean isPublicEvent) {
        this.title = title;
        this.image = image;
        this.video = video;
        this.description = description;
        this.category = category;
        this.location = location;
        this.eventDate = eventDate;
        this.eventAddedDate = Calendar.getInstance().getTime();
        this.isPublicEvent = isPublicEvent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEventAddedDate() {
        return eventAddedDate;
    }

    public void setEventAddedDate(Date eventAddedDate) {
        this.eventAddedDate = eventAddedDate;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public EventLocation getLocation() {
        return location;
    }

    public User getEventAddedBy() {
        return eventAddedBy;
    }

    public void setEventAddedBy(User eventAddedBy) {
        this.eventAddedBy = eventAddedBy;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setLocation(EventLocation location) {
        this.location = location;
    }

    public List<String> getSharedUserId() {
        return sharedUserId;
    }

    public void setSharedUserId(List<String> sharedUserId) {
        this.sharedUserId = sharedUserId;
    }

    public List<String> getLikedId() {
        return likedId;
    }

    public void setLikedId(List<String> likedId) {
        this.likedId = likedId;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public boolean isPublicEvent() {
        return isPublicEvent;
    }

    public void setPublicEvent(boolean publicEvent) {
        isPublicEvent = publicEvent;
    }
}