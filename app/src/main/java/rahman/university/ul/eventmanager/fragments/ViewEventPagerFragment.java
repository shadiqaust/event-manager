package rahman.university.ul.eventmanager.fragments;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.adapter.TabPagerItem;
import rahman.university.ul.eventmanager.adapter.ViewPagerAdapter;
import rahman.university.ul.eventmanager.models.Event;


public class ViewEventPagerFragment extends Fragment {
	private List<TabPagerItem> mTabs = new ArrayList<>();

    public static final String EVENT_KEY = "event";
    private Event event;

    // TODO: Rename and change types and number of parameters
    public static ViewEventPagerFragment newInstance(Event event) {
        ViewEventPagerFragment fragment = new ViewEventPagerFragment();
        Bundle args = new Bundle();
        args.putParcelable(EVENT_KEY, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            event = getArguments().getParcelable(EVENT_KEY);
        }
        createTabPagerItem();
    }

    private void createTabPagerItem(){
        mTabs.add(new TabPagerItem("Details", ViewEventFragment.newInstance(event)));
        mTabs.add(new TabPagerItem("Comments",EventCommentsFragment.newInstance(event)));
        mTabs.add(new TabPagerItem("Video", EventVideoFragment.newInstance(event)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_view_event_pager, container, false);
        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT ));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
    	ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);
    	
    	mViewPager.setOffscreenPageLimit(mTabs.size());
        mViewPager.setAdapter(new ViewPagerAdapter(getChildFragmentManager(), mTabs));
        TabLayout mSlidingTabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSlidingTabLayout.setElevation(15);
        }
        mSlidingTabLayout.setupWithViewPager(mViewPager);
    }


}