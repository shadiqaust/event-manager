package rahman.university.ul.eventmanager.fragments;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.activity.MainActivity;
import rahman.university.ul.eventmanager.models.Event;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EventVideoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EventVideoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventVideoFragment extends Fragment {

    public static final String EVENT_KEY = "event";
    private Event event;
    private VideoView videoView;
    FloatingActionButton eventEdit, eventDelete;
    private EventVideoFragment.OnFragmentInteractionListener mListener;

    FrameLayout videoFrame;
    TextView noVideoText;

    public EventVideoFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EventVideoFragment newInstance(Event event) {
        EventVideoFragment fragment = new EventVideoFragment();
        Bundle args = new Bundle();
        args.putParcelable(EVENT_KEY, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            event = getArguments().getParcelable(EVENT_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("View Event");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_event_video, container, false);

        findViews(rootView);
        setValue();
        if(!MainActivity.firebaseUser.getUid().equals(event.getEventAddedBy().getKey()))
        {
            eventEdit.hide();
            eventDelete.hide();
        }

        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT ));
        return rootView;
    }

    private void findViews(View rootView)
    {
        noVideoText = rootView.findViewById(R.id.noVideoText);
        videoFrame = rootView.findViewById(R.id.videoFrame);
        videoView = rootView.findViewById(R.id.videoView);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        MediaController mediaController = new MediaController(getContext());
                        mediaController.setPadding(0, 0, 0, 0);
                        mediaController.setAnchorView(videoView);
                        videoView.setMediaController(mediaController);
                    }
                });
            }
        });

        eventEdit = rootView.findViewById(R.id.eventEdit);

        eventEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = EditEventFragment.newInstance(event);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        eventDelete = rootView.findViewById(R.id.eventDelete);
        eventDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteEvent();
            }
        });

    }

    private void deleteEvent() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users/" + MainActivity.firebaseUser.getUid() + "/events/" + event.getKey());
        ref.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                Toast.makeText(getContext(), "Event is deleted.",
                        Toast.LENGTH_SHORT).show();
                gotoEventListFragment();


            }
        })
         .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Error!! Try Again ",
                                Toast.LENGTH_SHORT).show();
                    }
          });
    }

    private void gotoEventListFragment(){
        Fragment fragment = new ViewPagerFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setValue()
    {
        if(event.getVideo() == null || event.getVideo().equals(""))
        {
            noVideoText.setVisibility(View.VISIBLE);
            videoFrame.setVisibility(View.GONE);
        }else{
            noVideoText.setVisibility(View.GONE);
            try {
                if(!event.getVideo().equals(""))
                {
                    videoView.setVideoPath(event.getVideo());
                    videoView.seekTo( 1 );
                }

            }catch (Exception ex)
            {
                Log.d("Error","Invalid Video");
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
