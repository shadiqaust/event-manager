package rahman.university.ul.eventmanager.adapter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.models.Comment;
import rahman.university.ul.eventmanager.models.User;
import rahman.university.ul.eventmanager.utils.DateHelper;
import rahman.university.ul.eventmanager.viewholder.CommentViewHolder;

public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<CommentViewHolder> {

    Context context;
    FragmentActivity activity;
    List<Comment> comments;

    public CommentRecyclerViewAdapter(Context context, FragmentActivity activity, List<Comment> TempList) {
        this.comments = TempList;
        this.activity = activity;
        this.context = context;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_comment_row_new, parent, false);

        CommentViewHolder viewHolder = new CommentViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {
        Comment comment = comments.get(position);
        User user = comment.getUser();
        try{
            if(user.getProfilePicture() != null && !user.getProfilePicture().isEmpty())
            {
                Picasso.get().load(user.getProfilePicture()).into(holder.profilePicture);
            }
        }catch (Exception ex)
        {
            Log.d("Error","Invalid Image");
        }

        holder.name.setText(user.getName() == null ? "" : user.getName());
        holder.comment.setText(comment.getComments() == null ? "" : comment.getComments());
        holder.commentDate.setText(comment.getAddedDate() == null ? "" : DateHelper.dateToString( comment.getAddedDate()));

    }

    @Override
    public int getItemCount() {
        return comments.size();
    }
}