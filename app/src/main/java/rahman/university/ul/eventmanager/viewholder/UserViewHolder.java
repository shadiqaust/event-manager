package rahman.university.ul.eventmanager.viewholder;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.adapter.ClickListener;

public class UserViewHolder extends RecyclerView.ViewHolder {

    public TextView name, userEmail;
    public ImageView profilePicture;
    public CheckBox checkBox;

    public UserViewHolder(View itemView) {
        super(itemView);

        name = (TextView) itemView.findViewById(R.id.rTitleTv);
        userEmail = (TextView) itemView.findViewById(R.id.userEmail);
        profilePicture = (ImageView) itemView.findViewById(R.id.profile_Image);
        checkBox = (CheckBox) itemView.findViewById(R.id.select);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        });
    }

    private ClickListener mClickListener;


    public void setOnClickListener(ClickListener clickListener){
        mClickListener = clickListener;
    }
}