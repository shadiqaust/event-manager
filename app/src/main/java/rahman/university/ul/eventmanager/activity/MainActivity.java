package rahman.university.ul.eventmanager.activity;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import br.liveo.interfaces.OnItemClickListener;
import br.liveo.interfaces.OnPrepareOptionsMenuLiveo;
import br.liveo.model.HelpLiveo;
import br.liveo.navigationliveo.NavigationLiveo;
import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.fragments.EventCommentsFragment;
import rahman.university.ul.eventmanager.fragments.EventVideoFragment;
import rahman.university.ul.eventmanager.fragments.EventsFragment;
import rahman.university.ul.eventmanager.fragments.NewEventFragment;
import rahman.university.ul.eventmanager.fragments.UserListDialogFragment;
import rahman.university.ul.eventmanager.fragments.UserListFragment;
import rahman.university.ul.eventmanager.fragments.UserProfileFragment;
import rahman.university.ul.eventmanager.fragments.ViewEventFragment;
import rahman.university.ul.eventmanager.fragments.ViewPagerFragment;
import rahman.university.ul.eventmanager.models.User;
import rahman.university.ul.eventmanager.activity.usermanager.UserLoginActivity;

public class MainActivity extends NavigationLiveo implements OnItemClickListener,
        UserProfileFragment.OnFragmentInteractionListener,
        EventsFragment.OnFragmentInteractionListener,
        UserListFragment.OnFragmentInteractionListener,
        UserListDialogFragment.OnFragmentInteractionListener,
        EventCommentsFragment.OnFragmentInteractionListener,
        EventVideoFragment.OnFragmentInteractionListener,
        ViewEventFragment.OnFragmentInteractionListener {

    private HelpLiveo mHelpLiveo;
    private FirebaseAuth auth;
    public static FirebaseUser firebaseUser;
    public static User user;
    FirebaseDatabase mFirebaseDatabase;
    @Override
    public void onInt(Bundle savedInstanceState) {


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(MainActivity.this, UserLoginActivity.class));
            finish();
        }
        else{
            firebaseUser= auth.getCurrentUser();

            // User Information

            this.userPhoto.setImageResource(R.drawable.ic_no_user);
            this.userBackground.setImageResource(R.drawable.ic_user_background_first);

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference("users/"+firebaseUser.getUid());


            // Attach a listener to read the data at our posts reference
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    user = dataSnapshot.getValue(User.class);
                    user.setKey(dataSnapshot.getKey());
                    userName.setText(user.getName());
                    userEmail.setText(user.getEmail());
                    try{
                        if(user.getProfilePicture() != null && !user.getProfilePicture().isEmpty())
                        {
                            Picasso.get().load(user.getProfilePicture()).into(userPhoto);
                        }
                    }catch (Exception ex)
                    {
                        Log.d("Error","Invalid Image");
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });
        }


        // Creating items navigation
        mHelpLiveo = new HelpLiveo();
        mHelpLiveo.add(getString(R.string.event), R.drawable.ic_inbox_black_24dp);
        mHelpLiveo.add(getString(R.string.add_new_event), R.drawable.ic_add_white_24dp);
        mHelpLiveo.add("Users", R.drawable.user_icon);

        with(this).startingPosition(0) //Starting position in the list
                .addAllHelpItem(mHelpLiveo.getHelp())
                .colorItemSelected(R.color.nliveo_blue_colorPrimary) //State the name of the color, icon and meter when it is selected
                .footerItem(getString(R.string.logout), R.drawable.logout)
                .setOnClickUser(onClickPhoto)
                .setOnPrepareOptionsMenu(onPrepare)
                .setOnClickFooter(onClickFooter)
                .build();

       // int position = this.getCurrentPosition();
       // this.setElevationToolBar(position != 2 ? 15 : 0);

        getCurrentPlaceItems();
    }

    private void getCurrentPlaceItems() {
        if (!isLocationAccessPermitted()) {
            requestLocationAccessPermission();
        }
    }

    private boolean isLocationAccessPermitted() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    private void requestLocationAccessPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                1);
    }

    @Override
    public void onItemClick(int position) {
        Fragment mFragment = null;
        FragmentManager mFragmentManager = getSupportFragmentManager();

        switch (position){
            case 1:
                mFragment = NewEventFragment.newInstance();
                break;
            case 2:
                mFragment = UserListFragment.newInstance(true);
                break;
            default:
                mFragment = new ViewPagerFragment();
                break;

        }

        if (mFragment != null){
            mFragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
        }

        //setElevationToolBar(position != 2 ? 15 : 0);
    }

    private OnPrepareOptionsMenuLiveo onPrepare = new OnPrepareOptionsMenuLiveo() {
        @Override
        public void onPrepareOptionsMenu(Menu menu, int position, boolean visible) {
        }
    };

    private View.OnClickListener onClickPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment mFragment = null;
            FragmentManager mFragmentManager = getSupportFragmentManager();
            mFragment = UserProfileFragment.newInstance(user.getKey(),true);
            mFragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
            closeDrawer();
        }
    };

    private View.OnClickListener onClickFooter = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(MainActivity.this, UserLoginActivity.class));
            finish();
            closeDrawer();
        }
    };

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
