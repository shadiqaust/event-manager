package rahman.university.ul.eventmanager.adapter;

import android.view.View;

public interface ClickListener {
    void onItemClick(View view, int position);
}
