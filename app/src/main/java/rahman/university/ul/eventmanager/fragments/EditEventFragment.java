package rahman.university.ul.eventmanager.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.activity.MainActivity;
import rahman.university.ul.eventmanager.models.Event;
import rahman.university.ul.eventmanager.models.EventLocation;
import rahman.university.ul.eventmanager.utils.ToastHelper;

public class EditEventFragment extends Fragment {

    private static final int LOC_REQ_CODE = 1;
    private static final int ADDRESS_PICKER_REQUEST = 1020;
    private final int PICK_IMAGE_REQUEST = 71;
    private final int SELECT_VIDEO = 3;


    private Uri imagePath;
    private Uri videoPath;
    private ImageView imageView;
    TextInputEditText eventNameInput, eventDescriptionInput,eventLocation;
    DatePicker eventDate;
    TimePicker eventTime;
    FloatingActionButton eventSaveButton;
    Button btnChoose, selectPlaceBtn,btnVideoChoose;
    Spinner spinner;
    EventLocation location;
    VideoView eventVideo;
    CheckBox checkboxPublicEvent;
    private Event event;

    //Firebase
    FirebaseStorage storage;
    StorageReference storageReference;

    private static final String EVENT = "event";

    public static EditEventFragment newInstance(Event event){
        EditEventFragment mFragment = new EditEventFragment();
        Bundle mBundle = new Bundle();
        mBundle.putParcelable(EVENT, event);
        mFragment.setArguments(mBundle);
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            event = getArguments().getParcelable(EVENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("Update Event");
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_new_event, container, false);

        MapUtility.apiKey = getResources().getString(R.string.google_maps_key);

        spinner = (Spinner) rootView.findViewById(R.id.category_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.event_category_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        eventVideo = rootView.findViewById(R.id.eventVideoView);
        btnVideoChoose = rootView.findViewById(R.id.btnVideoChoose);
        checkboxPublicEvent = rootView.findViewById(R.id.checkboxPublicEvent);

        btnVideoChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseVideo();
            }
        });

        eventVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        MediaController mediaController = new MediaController(getContext());
                        mediaController.setPadding(0, 0, 0, 0);
                        mediaController.setAnchorView(eventVideo);
                        eventVideo.setMediaController(mediaController);
                    }
                });
            }
        });

        eventNameInput = (TextInputEditText) rootView.findViewById(R.id.eventName);
        eventDescriptionInput = (TextInputEditText) rootView.findViewById(R.id.eventDescription);
        eventLocation = (TextInputEditText) rootView.findViewById(R.id.eventLocation);
        eventSaveButton = (FloatingActionButton) rootView.findViewById(R.id.saveBtn);
        selectPlaceBtn = (Button) rootView.findViewById(R.id.selectPlaceBtn);
        eventSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEvent();
            }
        });

        btnChoose = (Button) rootView.findViewById(R.id.btnChoose);
        imageView = (ImageView) rootView.findViewById(R.id.eventImage);

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        selectPlaceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCurrentPlaceItems();
            }
        });

        eventDate = (DatePicker) rootView.findViewById(R.id.eventDate);
        eventTime = (TimePicker) rootView.findViewById(R.id.eventTime);

        setValue();

        rootView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT ));
        return rootView;
    }

    private void getCurrentPlaceItems() {
        if (isLocationAccessPermitted()) {
            showPlacePicker();
        } else {
            requestLocationAccessPermission();
        }
    }

    @SuppressLint("MissingPermission")
    private void showPlacePicker() {
        Intent intent = new Intent(getContext(), LocationPickerActivity.class);
        intent.putExtra(MapUtility.ADDRESS, location.getAddress());
        intent.putExtra(MapUtility.LATITUDE, location.getLatitude());
        intent.putExtra(MapUtility.LONGITUDE, location.getLongitude());
        startActivityForResult(intent, ADDRESS_PICKER_REQUEST);

    }

    private boolean isLocationAccessPermitted() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    private void requestLocationAccessPermission() {
        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                LOC_REQ_CODE);
    }



    private void setValue()
    {
        eventNameInput.setText(event.getTitle());
        eventDescriptionInput.setText(event.getDescription());
        spinner.setSelection(event.getCategory());
        checkboxPublicEvent.setChecked(event.isPublicEvent());
        if(event.getEventDate() != null)
        {
            Calendar c = Calendar.getInstance();
            c.setTime(event.getEventDate());

            eventDate.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE), null);
            eventTime.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
            eventTime.setCurrentMinute(c.get(Calendar.MINUTE));

        }
        location = event.getLocation();
        eventLocation.setText(event.getLocation().getAddress());

        try {
            if(event.getVideo() !=null && !event.getVideo().equals(""))
            {
                eventVideo.setVideoPath(event.getVideo());
                eventVideo.seekTo( 1 );
            }

        }catch (Exception ex)
        {
            Log.d("Error","Invalid Video");
        }

        try{
            Picasso.get().load(event.getImage()).into(imageView);

        }catch (Exception ex)
        {
            Log.d("Error","Invalid Image");
        }

    }


    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void chooseVideo(){
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Video "), SELECT_VIDEO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK
                && data != null && data.getData() != null )
        {
            imagePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), imagePath);
                imageView.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        if (requestCode == ADDRESS_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double currentLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double currentLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);
                    eventLocation.setText(address);
                    location = new EventLocation(address,currentLatitude,currentLongitude);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        if (requestCode == SELECT_VIDEO)
        {
            System.out.println("SELECT_VIDEO");
            videoPath = data.getData();

            eventVideo.setVideoURI(videoPath);
            eventVideo.seekTo(1);
        }
    }


    private void updateEvent(){


        if(eventNameInput.getText().toString().isEmpty() ||
                eventDescriptionInput.getText().toString().isEmpty() ||
                spinner.getSelectedItemPosition() < 0 ||
                location == null)
        {
            ToastHelper.showRedToast(getContext(),"Please fill all the required field.");
            return;
        }

        if(MainActivity.firebaseUser !=null)
        {

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("users/"+ MainActivity.firebaseUser.getUid()+"/events/"+event.getKey());

            Calendar calendar = new GregorianCalendar(eventDate.getYear(),
                    eventDate.getMonth(),
                    eventDate.getDayOfMonth(),
                    eventTime.getCurrentHour(),
                    eventTime.getCurrentMinute());

            event.setTitle(eventNameInput.getText().toString());
            event.setDescription(eventDescriptionInput.getText().toString());
            event.setCategory(spinner.getSelectedItemPosition());
            event.setImage(event.getImage() == null? "No image" : event.getImage());
            event.setLocation(location);
            event.setEventDate(calendar.getTime());
            event.setPublicEvent(checkboxPublicEvent.isChecked());

            ref.setValue(event)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {


                            if (videoPath != null) {
                                uploadVideo(ref);
                            }
                            if (imagePath != null) {
                                uploadImage(ref);
                            }else{
                                Toast.makeText(getContext(), "Event is updated.",
                                        Toast.LENGTH_SHORT).show();
                                gotoViewEventFragment();
                            }



                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), "Error: " + e.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
            Log.d("Ename",eventNameInput.getText().toString());
        }

    }

    private void gotoViewEventFragment(){
        Fragment fragment = ViewEventPagerFragment.newInstance(event);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void uploadImage(final DatabaseReference ref) {

        if(imagePath != null)
        {
            storage = FirebaseStorage.getInstance("gs://event-manager-eef75.appspot.com");
            storageReference = storage.getReference();

           final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            final StorageReference sRef = storageReference.child("images/"+ ref.getKey());
            sRef.putFile(imagePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            sRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    ref.child("image").setValue(uri.toString())
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    event.setImage(uri.toString());
                                                    Toast.makeText(getContext(), "Event is updated.",
                                                            Toast.LENGTH_SHORT).show();
                                                    gotoViewEventFragment();

                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(getContext(), "Error: " + e.getMessage(),
                                                            Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            });

                            progressDialog.dismiss();
                            Toast.makeText(getContext(), "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }

    private void uploadVideo(final DatabaseReference ref) {

        if(videoPath != null)
        {
            storage = FirebaseStorage.getInstance("gs://event-manager-eef75.appspot.com");
            storageReference = storage.getReference();

//            final ProgressDialog progressDialog = new ProgressDialog(getContext());
//            progressDialog.setTitle("Uploading...");
//            progressDialog.show();

            final StorageReference sRef = storageReference.child("videos/"+ ref.getKey());
            sRef.putFile(videoPath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            sRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    ref.child("video").setValue(uri.toString())
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    event.setVideo(uri.toString());


                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(getContext(), "Error: " + e.getMessage(),
                                                            Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                }
                            });

                            // progressDialog.dismiss();
                            //Toast.makeText(getContext(), "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // progressDialog.dismiss();
                            Toast.makeText(getContext(), "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                    .getTotalByteCount());
                            //  progressDialog.setMessage("Uploaded "+(int)progress+"%");
                        }
                    });
        }
    }




}
