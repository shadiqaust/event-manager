package rahman.university.ul.eventmanager.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.fragments.UserProfileFragment;
import rahman.university.ul.eventmanager.models.User;
import rahman.university.ul.eventmanager.viewholder.UserViewHolder;

public class UserRecyclerViewAdapter extends RecyclerView.Adapter<UserViewHolder> {

    Context context;
    FragmentActivity activity;
    List<User> users;
    Boolean isFromDialog = false;

    public UserRecyclerViewAdapter(Context context, FragmentActivity activity, List<User> TempList, Boolean isFromDialog) {
        this.users = TempList;
        this.activity = activity;
        this.context = context;
        this.isFromDialog = isFromDialog;
    }
    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_row, parent, false);
        UserViewHolder viewHolder = new UserViewHolder(view);
            viewHolder.setOnClickListener(new ClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    if(isFromDialog)
                    {
                        boolean checkStatus =  viewHolder.checkBox.isChecked();
                        viewHolder.checkBox.setChecked(!checkStatus);
                        users.get(position).setSelectedForShare(!checkStatus);
                        Toast.makeText(context, checkStatus ? "Remove":"Selected", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        gotoViewEventFragment(users.get(position));
                    }
                }
            });
        return viewHolder;
    }

    private void gotoViewEventFragment(User user){
        Fragment fragment = UserProfileFragment.newInstance(user.key,false);
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        User user = users.get(position);
        try{
            if(user.getProfilePicture() != null && !user.getProfilePicture().isEmpty())
            {
                Picasso.get().load(user.getProfilePicture()).into(holder.profilePicture);
            }
        }catch (Exception ex)
        {
            Log.d("Error","Invalid Image");
        }

        holder.name.setText(user.getName() == null ? "" : user.getName());
        holder.userEmail.setText(user.getEmail() == null ? "" : user.getEmail());
        if(isFromDialog){
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setClickable(false);
            if(user.isSelectedForShare())
            {
                holder.checkBox.setChecked(true);
            }
        }
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}