package rahman.university.ul.eventmanager.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.adapter.ClickListener;

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView title,eventDate;
    public ImageView eventImage;

    public ViewHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        });

        title = (TextView) itemView.findViewById(R.id.rTitleTv);
        eventDate = (TextView) itemView.findViewById(R.id.eventDate);
        eventImage = (ImageView) itemView.findViewById(R.id.rImageView);
    }

    private ClickListener mClickListener;

    public void setOnClickListener(ClickListener clickListener){
        mClickListener = clickListener;
    }
}