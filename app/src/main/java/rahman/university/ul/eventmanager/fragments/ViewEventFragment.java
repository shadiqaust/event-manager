package rahman.university.ul.eventmanager.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.List;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.activity.MainActivity;
import rahman.university.ul.eventmanager.models.Event;
import rahman.university.ul.eventmanager.utils.DateHelper;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViewEventFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViewEventFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewEventFragment extends Fragment {

    public static final String EVENT_KEY = "event";
    private TextView title;
    private TextView description;
    private TextView more;
    private View moreButtonGradient;
    private TextView time;
    private TextView numGoing;
    private TextView organization;
    private TextView location;
    private TextView category;
    private Event event;
    public ImageView image;
    FloatingActionButton like;
    MapView mapView;
    private NestedScrollView scrollView;
    private int descriptionLineCount = 0;

    FloatingActionButton share;

    FloatingActionButton eventEdit, eventDelete;

    private OnFragmentInteractionListener mListener;

    private boolean isLiked = false;

    public ViewEventFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ViewEventFragment newInstance(Event event) {
        ViewEventFragment fragment = new ViewEventFragment();
        Bundle args = new Bundle();
        args.putParcelable(EVENT_KEY, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            event = getArguments().getParcelable(EVENT_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("View Event");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_event, container, false);

        findViews(rootView);
        setValue();
        if(!MainActivity.firebaseUser.getUid().equals(event.getEventAddedBy().getKey()))
        {
            eventEdit.hide();
            share.hide();
            eventDelete.hide();
        }

        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT ));
        return rootView;
    }

    private void findViews(View rootView)
    {
        share = rootView.findViewById(R.id.share);
        like = rootView.findViewById(R.id.like);
        image = rootView.findViewById(R.id.image);
        title = rootView.findViewById(R.id.title);
        description = rootView.findViewById(R.id.description);
        category = rootView.findViewById(R.id.category);
        time = rootView.findViewById(R.id.time);
        numGoing = rootView.findViewById(R.id.numGoing);
        organization = rootView.findViewById(R.id.organization);
        location = rootView.findViewById(R.id.location);
        more = rootView.findViewById(R.id.more);
        moreButtonGradient = rootView.findViewById(R.id.moreButtonGradient);
        eventEdit = rootView.findViewById(R.id.eventEdit);
        eventDelete = rootView.findViewById(R.id.eventDelete);
        mapView =  rootView.findViewById(R.id.map_view);


        eventEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = EditEventFragment.newInstance(event);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        eventDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteEvent();
            }
        });

        rootView.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new ViewPagerFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Fragment prev = getFragmentManager().findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog.
                DialogFragment newFragment = UserListDialogFragment.newInstance(true, event);
                newFragment.show(ft, "dialog");
            }
        });

        if(event.getLikedId().contains(MainActivity.firebaseUser.getUid()))
        {
            like.setImageDrawable(getResources().getDrawable(R.drawable.thumb_up_active,null));
        }
        else{
            like.setImageDrawable(getResources().getDrawable(R.drawable.thumb_up,null));
        }

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likeEvent();
            }
        });

        scrollView = rootView.findViewById(R.id.scrollView);

        Rect rectangle = new Rect();
        Window window = getActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop =
                window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;
        image.setPadding(0, titleBarHeight, 0, 0);
    }


    private void deleteEvent() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users/" + MainActivity.firebaseUser.getUid() + "/events/" + event.getKey());
        ref.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                Toast.makeText(getContext(), "Event is deleted.",
                        Toast.LENGTH_SHORT).show();
                gotoEventListFragment();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Error!! Try Again ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void likeEvent(){
        if(MainActivity.firebaseUser !=null)
        {
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("users/"+ event.getEventAddedBy().getKey()+"/events/"+event.getKey());

            List<String> likeId = event.getLikedId();

            if(likeId.contains(MainActivity.firebaseUser.getUid()))
            {
                likeId.remove(MainActivity.firebaseUser.getUid());
                isLiked = false;
            }
            else {
                likeId.add(MainActivity.firebaseUser.getUid());
                isLiked = true;
            }

            ref.child("likes").setValue(likeId)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            if(isLiked)
                            {
                                like.setImageDrawable(getResources().getDrawable(R.drawable.thumb_up_active,null));
                                Toast.makeText(getContext(), "Liked.", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                like.setImageDrawable(getResources().getDrawable(R.drawable.thumb_up,null));
                                Toast.makeText(getContext(), "Disliked.", Toast.LENGTH_SHORT).show();
                            }
                            event.setLikedId(likeId);
                            numGoing.setText(event.getLikedId().size() + " Liked");
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), "Error: " + e.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        }

    }

    private void gotoEventListFragment(){
        Fragment fragment = new ViewPagerFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setValue()
    {
        title.setText(event.getTitle());
        description.setText(event.getDescription());
        category.setText(getResources().getStringArray(R.array.event_category_array)[event.getCategory()]);
        time.setText(DateHelper.dateToString(event.getEventDate()));
        numGoing.setText(event.getLikedId().size() + " Liked");
        organization.setText(event.getEventAddedBy().getName());

        location.setText(event.getLocation().getAddress());
        configureDescription();
        setMap(event.getLocation().getLatitude(), event.getLocation().getLongitude());
        try{
            Picasso.get().load(event.getImage()).into(image);
        }catch (Exception ex)
        {
            Log.d("Error","Invalid Image");
        }

    }


    //this method is to show map
    public void setMap(final double latitude, final double longitude){
        mapView.onCreate(null);
        mapView.onResume();
        mapView.getMapAsync(
                new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googlemap) {
                        final GoogleMap map = googlemap;

                        MapsInitializer.initialize(getContext());
                        //change map type as your requirements
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        //user will see a blue dot in the map at his location
                        map.setMyLocationEnabled(true);
                        LatLng marker =new LatLng(latitude, longitude);

                        //move the camera default animation
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 8));

                        //add a default marker in the position
                        map.addMarker(new MarkerOptions().title(event.getLocation().getAddress())
                                .position(marker)).showInfoWindow();

                    }
                }
        );
    }

    private void configureDescription()
    {
        //find out how many lines the description text will be
        description.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
        {
            @Override
            public boolean onPreDraw()
            {
                if (more.getVisibility() != View.VISIBLE)
                    return true;
                // save description line count only first time
                int lineCount = description.getLayout().getLineCount();
                if (descriptionLineCount > lineCount)
                    return true;

                descriptionLineCount = lineCount;
                if (descriptionLineCount >= 5)
                {
                    description.setLines(5);
                    more.setVisibility(View.VISIBLE);
                    moreButtonGradient.setVisibility(View.VISIBLE);
                }
                else
                {
                    description.setLines(descriptionLineCount);
                    more.setVisibility(View.GONE);
                    moreButtonGradient.setVisibility(View.GONE);
                }
                return true;
            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
