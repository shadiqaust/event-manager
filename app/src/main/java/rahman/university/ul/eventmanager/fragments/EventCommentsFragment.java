package rahman.university.ul.eventmanager.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.activity.MainActivity;
import rahman.university.ul.eventmanager.adapter.CommentRecyclerViewAdapter;
import rahman.university.ul.eventmanager.models.Comment;
import rahman.university.ul.eventmanager.models.Event;
import rahman.university.ul.eventmanager.models.User;
import rahman.university.ul.eventmanager.utils.DateHelper;
import rahman.university.ul.eventmanager.utils.ToastHelper;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EventCommentsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EventCommentsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventCommentsFragment extends Fragment {

    public static final String EVENT_KEY = "event";
    private Event event;
    Button commentButton;
    EditText commentText;
    private NestedScrollView scrollView;
    private int descriptionLineCount = 0;

    RecyclerView mRecyclerView;
    RecyclerView.Adapter adapter ;
    User user;

    FloatingActionButton eventEdit, eventDelete;

    private OnFragmentInteractionListener mListener;

    private boolean isLiked = false;

    public EventCommentsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static EventCommentsFragment newInstance(Event event) {
        EventCommentsFragment fragment = new EventCommentsFragment();
        Bundle args = new Bundle();
        args.putParcelable(EVENT_KEY, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            event = getArguments().getParcelable(EVENT_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("View Event");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_event_comments, container, false);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userRef = database.getReference("users/"+MainActivity.firebaseUser.getUid());

        // Attach a listener to read the data at our posts reference
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
                user.setKey(dataSnapshot.getKey());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });



        findViews(rootView);
        setValue();
        if(!MainActivity.firebaseUser.getUid().equals(event.getEventAddedBy().getKey()))
        {
            eventEdit.hide();
            eventDelete.hide();
        }

        rootView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT ));
        return rootView;
    }

    private void findViews(View rootView)
    {

        eventEdit = rootView.findViewById(R.id.eventEdit);
        eventDelete = rootView.findViewById(R.id.eventDelete);

        commentButton = rootView.findViewById(R.id.commentButton);
        commentText =  rootView.findViewById(R.id.commentText);

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comment();
            }
        });


        eventEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = EditEventFragment.newInstance(event);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        eventDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteEvent();
            }
        });



        //RecyclerView
        mRecyclerView = rootView.findViewById(R.id.commentRecyclerView);
        mRecyclerView.setHasFixedSize(true);

        //set layout as LinearLayout
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

    }


    private void deleteEvent(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("users/"+ MainActivity.firebaseUser.getUid()+"/events/"+event.getKey());
        ref.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                Toast.makeText(getContext(), "Event is deleted.",
                        Toast.LENGTH_SHORT).show();
                gotoEventListFragment();


            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getContext(), "Error!! Try Again ",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void comment(){

        if(commentText.getText().toString().isEmpty())
        {
            ToastHelper.showRedToast(getContext(),"Please add your comment.");
            return;
        }

        if(MainActivity.firebaseUser !=null )
        {
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("users/"+ event.getEventAddedBy().getKey()+"/events/"+event.getKey());
            List<Comment> comments = event.getComments();
                     
            comments.add(new Comment(user,commentText.getText().toString()));

            ref.child("comments").setValue(comments)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            commentText.setText("");
                            Toast.makeText(getContext(), "Comment is added.", Toast.LENGTH_SHORT).show();
                            event.setComments(comments);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), "Error: " + e.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void gotoEventListFragment(){
        Fragment fragment = new ViewPagerFragment();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setValue()
    {
        adapter = new CommentRecyclerViewAdapter(getContext(),getActivity(), event.getComments());
        mRecyclerView.setAdapter(adapter);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
