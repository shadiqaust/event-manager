package rahman.university.ul.eventmanager.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import rahman.university.ul.eventmanager.R;
import rahman.university.ul.eventmanager.activity.MainActivity;
import rahman.university.ul.eventmanager.adapter.UserRecyclerViewAdapter;
import rahman.university.ul.eventmanager.models.Event;
import rahman.university.ul.eventmanager.models.User;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserListDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserListDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserListDialogFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "isAllUser";
    private static final String ARG_PARAM2 = "event";

    // TODO: Rename and change types of parameters
    private boolean isAllUser;

    private OnFragmentInteractionListener mListener;

    RecyclerView mRecyclerView;
    FirebaseDatabase mFirebaseDatabase;
    Query mRef;
    List<User> users = new ArrayList<>();
    RecyclerView.Adapter adapter ;

    FloatingActionButton share, back;

    Event event;


    public UserListDialogFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param isAllUser Parameter 1.
     * @return A new instance of fragment EventsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserListDialogFragment newInstance(boolean isAllUser, Event event) {
        UserListDialogFragment fragment = new UserListDialogFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, isAllUser);
        args.putParcelable(ARG_PARAM2, event);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL,
                R.style.AppTheme);

        if (getArguments() != null) {
            isAllUser = getArguments().getBoolean(ARG_PARAM1);
            event = getArguments().getParcelable(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().setTitle("User List");

        // Inflate the layout for this fragment
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_event_sharing, container, false);

        share = rootView.findViewById(R.id.share);
        back = rootView.findViewById(R.id.cancel_action);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();

            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareEvent();
            }
        });

        mRecyclerView = rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        //set layout as LinearLayout
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //send Query to FirebaseDatabase
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mFirebaseDatabase.getReference("users");

        users= new ArrayList<>();
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                users.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    if(!MainActivity.firebaseUser.getUid().equals(postSnapshot.getKey()))
                    {
                        User user = postSnapshot.getValue(User.class);
                        user.setKey(postSnapshot.getKey());
                        if(event.getSharedUserId().contains(postSnapshot.getKey()))
                        {
                            user.setSelectedForShare(true);
                        }
                        users.add(user);
                    }
                }
                adapter = new UserRecyclerViewAdapter(getContext(), getActivity(), users,true);
                mRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });

        return rootView;
    }

    private void shareEvent(){

        if(MainActivity.firebaseUser !=null)
        {

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("users/"+ MainActivity.firebaseUser.getUid()+"/events/"+event.getKey());

            List<String> sharedUserIds = new ArrayList<>();

            for (User user : users) {
                if(user.isSelectedForShare())
                {
                    sharedUserIds.add(user.getKey());
                }
            }

            ref.child("sharedUserId").setValue(sharedUserIds)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //gotoViewEventFragment(event);
                            event.setSharedUserId(sharedUserIds);
                            getDialog().dismiss();
                            Toast.makeText(getContext(), "Event is shared.", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getContext(), "Error: " + e.getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    //search data
    private void firebaseSearch(String searchText) {
        List<User> userList = new ArrayList<>();
        for (User  user : users) {
            if(user.getName() != null && user.getName().toLowerCase().contains(searchText.toLowerCase()))
            {
                userList.add(user);
            }
        }
        adapter = new UserRecyclerViewAdapter(getContext(),getActivity(), userList,true );
        mRecyclerView.setAdapter(adapter);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Auto-generated method stub
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);

        //Select search item
        final MenuItem menuItem = menu.findItem(R.id.menu_search);
        menuItem.setVisible(true);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setQueryHint(this.getString(R.string.search));

        ((EditText) searchView.findViewById(R.id.search_src_text))
                .setHintTextColor(getResources().getColor(R.color.nliveo_white));
        searchView.setOnQueryTextListener(onQuerySearchView);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }


    private SearchView.OnQueryTextListener onQuerySearchView = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            firebaseSearch(query == null ? "" : query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            //Filter as you type
            firebaseSearch(newText == null ? "" : newText);
            return false;
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub

        switch (item.getItemId()) {
            case R.id.menu_search:
                Toast.makeText(getActivity(), R.string.search, Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }


}
